package omh.testapplication.kotlin

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import omh.testapplication.R

/* TODO:
    * Task 2) Photocopy machine!
    *
    *  --- Description ---
    *  Study the image shown in the activity, task2.png
    *  and replicate the design into layouts and views.
    *  There is no restriction on how to replicate the design use your
    *  creativity :)
    *
    *  --- Requirements ---
    * - Left/Right swipeable calendar
    * - Bottom slideable overlay + expandable listview
    * - Follow the UI design as closely as possible
    * - Naming conventions have to be clear and readable
    * - Code has to be neat and organize
    * - Create any custom files/classes you want, edit file activity_task2.xml
    * - Any missing assets please get from https://icons8.com/
    * - Incorrect icons/ images will not be penalised
    *
    * --- References ---
    * - Google Calendar
    *
    * --- Plus Points ---
    * - Good UX
    * - Using newer layouts, i.e. ConstraintLayout, CoordinatorLayout
    *
    * */


class Task2 : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_task2)

    }

}