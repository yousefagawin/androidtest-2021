package omh.testapplication;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

/* TODO:
    * Task 3) StarWars!
    *  --- Description ---
    *  This tasks requires you to make api calls and understand JSON serialization.
    *  You will be working with public apis and handling local data storage as well.
    *  Create 2 seperate tabs for this task.
    *  Tab 1:
    *       - Display List of Characters
    *       - Enable search function to search specific/ filter  Characters
    *       - Able to favourite/unfavourite any Character
    *  Tab 2:
    *       - Show favourited Characters
    *       - Able to unfavourite Characters
    *
    *  --- Requirements ---
    *  - Make use of the api https://swapi.dev/api/people/ to get the list of characters.
    *       - Response will be paginated, so only load results on demand.
    *  - Make use of the api https://swapi.dev/api/people/?search={val} to search for characters
    *       - Replace {val} with any searcheable value e.g. luke
    *  - Favourite/Unfavourite any star wars character
    *  - Favourited characters will be stored locally
    *  - Characters Required field to display: name, gender, height, mass, birthday
    *  - REQUIRED for Senior Roles: Apply pagination and auto-load the next results upon reaching the last item of the page
    *
    *  - Naming conventions have to be clear and readable
    *  - Code has to be neat and organize
    *  - Create any custom files/classes you want, edit file activity_task3.xml
    *  - UI/UX will not be penalized, but having a great UI is a bonus!
    *
    *  Preferred libraries to work with - retrofit, realm/room, paging library
    *
    *  --- References ---
    *  API host endpoint : https://swapi.dev/api/
    *  Test apis @ https://swapi.dev/
    *  Documentation @ https://swapi.dev/documentation
    *
    * */


public class Task3 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task3);
    }

}
