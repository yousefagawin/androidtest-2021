# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This test application requires you to: 
 - Apply your prefered Android Architecture (MVVM, MVC, etc.)
 - Apply any of your preferred Android Dependency Injection
 - Apply Databinding or Viewbinding (bonus points but not required)

The instructions of individual task are found at the top of the 3 activities:
Task1.java (1. Optimization) 
Task2.java (2. Following design)
Task3.java (3. Network and local storage)

or if you are familiar with Kotlin you can work on:
kotlin/Task1.kt
kotlin/Task2.kt
kotlin/Task3.kt

Please read properly and perform them to your best abilities.

This codebase is only distributed to authorize personnel.
Any redistribution or sharing of codebase is strictly prohibited.

### How do I get set up? ###
This code is written in Android Studio 4.1.2 and it is recommended to be open with Android Studio.
Run the code onto an android device/ emulator.

### Contribution guidelines ###
Please fork from this repository and not to commit directly to the master branch.
Make your repository private and invite francis@ohmyhome.sg to it.

### Who do I talk to? ###
Any issues you face along the way please contact me @ francis@ohmyhome.com